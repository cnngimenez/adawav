--  riffs-wavs-formats.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Characters.Latin_1;

package body RIFFs.WAVs.Formats is

    function Get_Tag (Chunk : Chunk_Data; From : Positive)
        return Tag_Type
    is
    begin
        if From + 1 > Chunk'Length then
            return Unknown;
        end if;

        case  Join_Bytes (Chunk (From), Chunk (From + 1))  is
        when 16#0001# =>
            return PCM;
        when 16#0003# =>
            return IEEE_Float;
        when 16#0006# =>
            return A_Law;
        when 16#0007# =>
            return Mu_Law;
        when 16#FFFE# =>
            return Extensible;
        when others =>
            return Unknown;
        end case;
    end Get_Tag;

    function Get_Unsigned_16 (Chunk : Chunk_Data; From : Positive)
        return Unsigned_16
    is
    begin
        if From + 1 > Chunk'Length then
            return 0;
        end if;

        return Join_Bytes (Chunk (From), Chunk (From + 1));
    end Get_Unsigned_16;

    function Get_Unsigned_32 (Chunk : Chunk_Data; From : Positive)
        return Unsigned_32
    is
    begin
        if From + 3 > Chunk'Length then
            return 0;
        end if;

        return Join_Bytes (Chunk (From), Chunk (From + 1),
                           Chunk (From + 2), Chunk (From + 3));
    end Get_Unsigned_32;

    function Join_Bytes (B1, B2 : Byte) return Unsigned_16 is
        Ret : Unsigned_16;
    begin
        Ret := Unsigned_16 (B1);
        Ret := Shift_Left (Ret, 4) and 16#F0#;
        Ret := Ret or Unsigned_16 (B2);

        return Ret;
    end Join_Bytes;

    function Join_Bytes (B1, B2, B3, B4 : Byte) return Unsigned_32 is
        Ret : Unsigned_32;
    begin
        Ret := Unsigned_32 (B1);
        Ret := Shift_Left (Ret, 4) and 16#FFF0#;
        Ret := Ret or Unsigned_32 (B2);

        Ret := Shift_Left (Ret, 4) and 16#FFF0#;
        Ret := Ret or Unsigned_32 (B3);

        Ret := Shift_Left (Ret, 4) and 16#FFF0#;
        Ret := Ret or Unsigned_32 (B4);

        return Ret;
    end Join_Bytes;

    procedure Read_Format (Riff : RIFF_File; Format : out Format_Type) is
        Chunk_Number : Natural;
    begin
        Chunk_Number := Find_Chunk_Number (Riff, "fmt ");
        if Chunk_Number = 0 then
            --  Chunk not found!
            Format := Empty_Format;
            return;
        end if;

        Read_Format (Nth_Chunk_Data (Riff, Chunk_Number), Format);
    end Read_Format;

    procedure Read_Format (Chunk : Chunk_Data; Format : out Format_Type) is
    begin
        Format.Tag := Get_Tag (Chunk, 0 + 1);
        Format.Channels := Get_Unsigned_16 (Chunk, 2 + 1);
        Format.Sampling_Rate := Get_Unsigned_32 (Chunk, 6 + 1);
        Format.Data_Rate := Get_Unsigned_32 (Chunk, 16#A# + 1);
        Format.Block_Size := Get_Unsigned_16 (Chunk, 16#C# + 1);
        Format.Bits_Per_Sample := Get_Unsigned_16 (Chunk, 16#E# + 1);
        Format.Extension_Size := Get_Unsigned_16 (Chunk, 16#10# + 1);
        Format.Valid_Bits := Get_Unsigned_16 (Chunk, 16#12# + 1);
        Format.Speaker_Mask := Get_Unsigned_32 (Chunk, 16#14# + 1);
        --  Subformat : Guid_Type;
    end Read_Format;

    function To_String (Format : Format_Type) return String is
        Eol : constant Character := Ada.Characters.Latin_1.LF;
    begin
        return "- WAV Format:" & Eol
            & "  Tag: " & Format.Tag'Image & Eol
            & "  Channels: " & Format.Channels'Image & Eol
            & "  Sampling Rate: "  & Format.Sampling_Rate'Image & Eol
            & "  Data Rate: " & Format.Data_Rate'Image & Eol
            & "  Block Size: " & Format.Block_Size'Image & Eol
            & "  Bits Per Sample: " & Format.Bits_Per_Sample'Image & Eol
            & "  Extension Size: " & Format.Extension_Size'Image & Eol
            & "  Valid Bits: " & Format.Valid_Bits'Image & Eol
            & "  Speaker Mask: " & Format.Speaker_Mask'Image & Eol;
            --  & "  Subformat: " & Format.Subformat'Image & Eol;
    end To_String;

end RIFFs.WAVs.Formats;
