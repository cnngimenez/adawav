--  riffs-wavs-facts.adb ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

package body RIFFs.WAVs.Facts is
    function Join_Bytes (B1, B2, B3, B4 : Byte) return Unsigned_32;
    function Get_Unsigned_32 (Chunk : Chunk_Data; From : Positive)
            return Unsigned_32;

    function Get_Unsigned_32 (Chunk : Chunk_Data; From : Positive)
        return Unsigned_32
    is
    begin
        if From + 3 > Chunk'Length then
            return 0;
        end if;

        return Join_Bytes (Chunk (From), Chunk (From + 1),
                           Chunk (From + 2), Chunk (From + 3));
    end Get_Unsigned_32;

    function Join_Bytes (B1, B2, B3, B4 : Byte) return Unsigned_32 is
        Ret : Unsigned_32;
    begin
        Ret := Unsigned_32 (B1);
        Ret := Shift_Left (Ret, 4) and 16#FFF0#;
        Ret := Ret or Unsigned_32 (B2);

        Ret := Shift_Left (Ret, 4) and 16#FFF0#;
        Ret := Ret or Unsigned_32 (B3);

        Ret := Shift_Left (Ret, 4) and 16#FFF0#;
        Ret := Ret or Unsigned_32 (B4);

        return Ret;
    end Join_Bytes;

    function Read_Samples_Per_Channel (Riff : RIFF_File)
        return Positive
    is
        Chunk_Number : Natural;
    begin
        Chunk_Number := Find_Chunk_Number (Riff, "fact");
        if Chunk_Number = 0 then
            raise Facts_Chunk_Not_Found_Exception;
        end if;

        return Read_Samples_Per_Channel (Nth_Chunk_Data (Riff, Chunk_Number));
    end Read_Samples_Per_Channel;

    function Read_Samples_Per_Channel (Chunk : Chunk_Data)
        return Positive
    is
    begin
        if Chunk'Length /= 4 then
            raise Facts_Chunk_Wrong_Length_Exception;
        end if;

        return Positive (Get_Unsigned_32 (Chunk, 1));
    end Read_Samples_Per_Channel;

end RIFFs.WAVs.Facts;
