--  riffs-data_reader.ads ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

generic
    type Datum_Type is private;
    Empty_Datum : Datum_Type;
package RIFFs.Data_Readers is

    type Chunk_Data is array (Positive range <>) of Datum_Type;
    Empty_Chunk_Data : constant Chunk_Data := (Empty_Datum, Empty_Datum);

    function Nth_Chunk_Data (Riff : RIFF_File;
                             Chunk_Number : Positive)
                             return Chunk_Data;
    --  Return all data inside a Chunk.
    --
    --  Warning: it can be too Big!

    function Nth_Chunk_Data (Riff : RIFF_File;
                             Chunk_Number : Positive;
                             From, To : Positive)
                             return Chunk_Data;
    --  Return a partial data from the Chunk.
    --
    --  This function is safer, as it returns a specific portion of Data
    --  that is inside the specified Chunk.

private
    function Read_Bytes (Input_Stream : Stream_Access;
                         Amount : Unsigned_32)
                         return Chunk_Data;

end RIFFs.Data_Readers;
