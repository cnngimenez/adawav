--  RIFFs-WAVs-Data-PCM.adb ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

package body RIFFs.WAVs.Data.PCMs is

    function Read_Samples (Input : in out RIFF_File;
                           From, To : Positive)
                           return PCM16_Data_Type
    is
        Data : PCM16_Data_Type (1 .. To - From + 1);

        Current_Index : Positive := From;
        I : Positive := 1;

        Input_Stream : Stream_Access;
        Datum : PCM16;
    begin
        if To < From then
            return Empty_PCM16_Data;
        end if;

        Input_Stream := Stream (Input.File);
        Set_Index (Input.File, Index (Input.File) + Positive_Count (From));

        while not End_Of_File (Input_File) and then Current_Index <= To
        loop
            PCM16'Read (Input_Stream, Datum);
            Data (I) := Datum;

            I := I + 1;
            Current_Index := Current_Index + 1;
        end loop;

        return Data;
    end Read_Samples;

    function Read_Samples (Input : in out RIFF_File;
                           From, To : Positive)
                           return PCM24_Data_Type
    is
        Data : PCM24_Data_Type (1 .. To - From + 1);

        Current_Index : Positive := From;
        I : Positive := 1;

        Input_Stream : Stream_Access;
        Datum : PCM24;
    begin
        if To < From then
            return Empty_PCM24_Data;
        end if;

        Input_Stream := Stream (Input.File);
        Set_Index (Input.File, Index (Input.File) + Positive_Count (From));

        while not End_Of_File (Input_File) and then Current_Index <= To
        loop
            PCM24'Read (Input_Stream, Datum);
            Data (I) := Datum;

            I := I + 1;
            Current_Index := Current_Index + 1;
        end loop;

        return Data;
    end Read_Samples;

    function Read_Samples (Input : in out RIFF_File;
                           From, To : Positive)
                           return PCM32_Data_Type
    is
        Data : PCM32_Data_Type (1 .. To - From + 1);

        Current_Index : Positive := From;
        I : Positive := 1;

        Input_Stream : Stream_Access;
        Datum : PCM32;
    begin
        if To < From then
            return Empty_PCM32_Data;
        end if;

        Input_Stream := Stream (Input.File);
        Set_Index (Input.File, Index (Input.File) + Positive_Count (From));

        while not End_Of_File (Input_File) and then Current_Index <= To
        loop
            PCM32'Read (Input_Stream, Datum);
            Data (I) := Datum;

            I := I + 1;
            Current_Index := Current_Index + 1;
        end loop;

        return Data;
    end Read_Samples;

end RIFFs.WAVs.Data.PCMs;
