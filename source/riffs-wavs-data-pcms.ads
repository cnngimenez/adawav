--  RIFFs-WAVs-Data-PCM.ads ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

package RIFFs.WAVs.Data.PCMs is

    --  Three types are defined and not a generic one because we Want
    --  to provide support for specific PCM sizes.  For Example,
    --  only 16, 24, and 32 bit sizes, but not for 8 nor 50 bits.

    type PCM16 is mod 2**16;
    type PCM24 is mod 2**24;
    type PCM32 is mod 2**32;

    type PCM16_Data_Type is array (Positive range <>) of PCM16;
    Empty_PCM16_Data : constant PCM16_Data_Type := (0, 0);
    type PCM24_Data_Type is array (Positive range <>) of PCM24;
    Empty_PCM24_Data : constant PCM24_Data_Type := (0, 0);
    type PCM32_Data_Type is array (Positive range <>) of PCM32;
    Empty_PCM32_Data : constant PCM32_Data_Type := (0, 0);

    function Read_Samples (Input : in out RIFF_File;
                           From, To : Positive)
                           return PCM16_Data_Type;
    function Read_Samples (Input : in out RIFF_File;
                           From, To : Positive)
                           return PCM24_Data_Type;
    function Read_Samples (Input : in out RIFF_File;
                           From, To : Positive)
                           return PCM32_Data_Type;



end RIFFs.WAVs.Data.PCMs;
