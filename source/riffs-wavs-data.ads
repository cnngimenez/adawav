--  riffs-wavs-data.ads ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

package RIFFs.WAVs.Data is

    type Float_Data_Type is array (Positive range <>) of Float;
    Empty_Float_Data : constant Float_Data_Type := (0.0, 0.0);

    function Get_Samples_Float (Input : in out RIFF_File;
                                From, To : Positive)
                                return Float_Data_Type;
    --  Return sample data provided by the given Indexes.
    --
    --  It is red from the Riff file Directly.
    --  Float means: bits per sample = 32 (4 Bytes).
    --  From and To must be in Bytes.

    function Read_One_Channel_Floats (Input : in out RIFF_File;
                                      Channel : Positive)
                                      return Float_Data_Type;
end RIFFs.WAVs.Data;
