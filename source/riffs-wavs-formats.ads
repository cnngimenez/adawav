--  wav-formats.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

package RIFFs.WAVs.Formats is
    type Tag_Type is (Unknown, PCM, IEEE_Float, A_Law, Mu_Law, Extensible);
    --  Type of WAVs Data.
    --  Unknown is used when an invalid or not defined type is used.

    for Tag_Type'Size use 16;
    for Tag_Type use (
                      Unknown => 16#0000#,
                      PCM => 16#0001#,
                      IEEE_Float => 16#0003#,
                      A_Law => 16#0006#,
                      Mu_Law => 16#0007#,
                      Extensible => 16#fffe#
                     );

    type Guid_Type is array (1 .. 16) of Unsigned_8;
    Empty_Guid : constant Guid_Type := (
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0
    );

    type Format_Type is record
        Tag : Tag_Type := Unknown;
        Channels : Unsigned_16;
        Sampling_Rate : Unsigned_32;
        Data_Rate : Unsigned_32;
        Block_Size : Unsigned_16;
        Bits_Per_Sample : Unsigned_16;
        Extension_Size : Unsigned_16;
        Valid_Bits : Unsigned_16;
        Speaker_Mask : Unsigned_32;
        Subformat : Guid_Type;
    end record
    with Pack;

    Empty_Format : constant Format_Type := (
        Tag => Unknown,
        Channels => 0,
        Sampling_Rate => 0,
        Data_Rate => 0,
        Block_Size => 0,
        Bits_Per_Sample => 0,
        Extension_Size => 0,
        Valid_Bits => 0,
        Speaker_Mask => 0,
        Subformat => Empty_Guid
        );
    procedure Read_Format (Riff : RIFF_File; Format : out Format_Type);
    --  Search the fmt chunk and process it into a Format_Type.
    --  Use Riff.Open on Riff before using this procedure.

    procedure Read_Format (Chunk : Chunk_Data; Format : out Format_Type);
    --  Read a Chunk_Data into a Format_Type.

    function To_String (Format : Format_Type) return String;
    --  Return a more human-readable string with the Format Information.

private
    --  function Get_Unsigned_16 (Chunk : Chunk_Data; From : Positive)
    --          return Unsigned_16;

    --  function Get_Unsigned_32 (Chunk : Chunk_Data; From : Positive)
    --      return Unsigned_32;

    function Join_Bytes (B1, B2 : Byte) return Unsigned_16;

    function Join_Bytes (B1, B2, B3, B4 : Byte) return Unsigned_32;

    function Get_Tag (Chunk : Chunk_Data; From : Positive)
        return Tag_Type;

    function Get_Unsigned_16 (Chunk : Chunk_Data; From : Positive)
        return Unsigned_16;

    function Get_Unsigned_32 (Chunk : Chunk_Data; From : Positive)
        return Unsigned_32;
end RIFFs.WAVs.Formats;
