--  wavs-data.adb ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with RIFFs.WAVs.Formats;
with RIFFs.WAVs.Facts;

package body RIFFs.WAVs.Data is

    function Get_Samples_Float (Input : in out RIFF_File;
                                From, To : Positive)
                                return Float_Data_Type
    is
        function Is_Float return Boolean;

        Format : RIFFs.WAVs.Formats.Format_Type;

        function Is_Float return Boolean is
            use RIFFs.WAVs.Formats;
        begin
            return Format.Tag = IEEE_Float
                and then Format.Bits_Per_Sample = 32;
        end Is_Float;

        Data : Float_Data_Type (1 .. To - From + 1);
        Current_Index : Positive := From;
        --  Current index of red samples from the file.
        I : Positive := 1;
        --  Index of samples (float) data in the Data array.
        Input_Stream : Stream_Access;
        Datum : Float;
        --  Bytes_To_Skip : Positive;
        --  Bytes to skip after the channel is Red.
        --  Used to skip all the other channel we are not interested.
    begin
        if To < From then
            return Empty_Float_Data;
        end if;

        --  RIFFs.WAVs.Formats.Read_Format (Input, Format);
        --  if not Is_Float then
        --      raise Not_Float_Data_Exception;
        --  end if;

        --  Bytes_To_Skip := Samples_Per_Channel * (Format.Channels - 1) * 4;
        --  Each sample is 32 bits = 4 bytes

        Input_Stream := Stream (Input.File);
        Set_Index (Input.File, Index (Input.File) + Positive_Count (From));

        while not End_Of_File (Input.File)
            and then Current_Index <= To
        loop
            Float'Read (Input_Stream, Datum);
            Data (I) := Datum;
            I := I + 1;
            Current_Index := Current_Index + 1;
        end loop;

        return Data;
    end Get_Samples_Float;

    function Read_One_Channel_Floats (Input : in out RIFF_File;
                                      Channel : Positive)
                                      return Float_Data_Type
    is
        procedure Goto_Channel_Position;
        --  Goto the channel position in the file.

        Samples_Per_Channel : constant Positive :=
            RIFFs.WAVs.Facts.Read_Samples_Per_Channel (Input);
        Input_Stream : Stream_Access;
        Sample_Count : Positive := 1;
        Datum : Float;
        Data : Float_Data_Type (1 .. Samples_Per_Channel + 1);

        procedure Goto_Channel_Position is
            Data_Starting_Position : Count;
            Channel_Position : constant Positive
                := Samples_Per_Channel * (Channel - 1) * 4;
        begin
            Data_Starting_Position := RIFFs.Chunk_Data_Address (Input, "data");
            Set_Index (Input.File,
                Data_Starting_Position + Count (Channel_Position));
        end Goto_Channel_Position;

    begin
        Input_Stream := Stream (Input.File);

        Goto_Channel_Position;

        while not End_Of_File (Input.File)
            and then Sample_Count <= Samples_Per_Channel
        loop
            Float'Read (Input_Stream, Datum);

            Data (Sample_Count) := Datum;
            Sample_Count := Sample_Count + 1;
        end loop;

        return Data;
    end Read_One_Channel_Floats;

end RIFFs.WAVs.Data;
