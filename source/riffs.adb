--  riffs.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Characters.Latin_1;

package body RIFFs is

    function Chunk_Address (Riff : RIFF_File;
                            Chunk_Number : Positive) return Unsigned_32 is
        Address : Unsigned_32 := 16#000C#;
        Index : Positive := 1;
        Header : Chunk_Header;
    begin
        while Index < Chunk_Number loop
            Header := Nth_Chunk_Header (Riff, Index);
            --  New address is chunk header length + chunk data length.
            Address := Address + 8 + Header.Length;
            Index := Index + 1;
        end loop;

        return Address;
    end Chunk_Address;

    function Chunk_Address (Riff : RIFF_File;
                            Chunk_Number : Positive) return Count
    is
        Address : Unsigned_32 := 0;
    begin
        Address := Chunk_Address (Riff, Chunk_Number);
        return Count (Address);
        --  This way, Ada is forced to use Chunk_Address that Returns
        --  Unsigned_32.
        --
        --  Don't try the next one, it won't work (Ada thinks it's a Recursive
        --   call).
        --  return Count (Chunk_Address (Riff, Chunk_Number));
    end Chunk_Address;

    function Chunk_Count (Riff : RIFF_File) return Natural is
        use Chunk_Header_Package;
    begin
        return Natural (Length (Riff.Chunk_Headers));
    end Chunk_Count;

    function Chunk_Data_Address (Riff : RIFF_File;
                                 Type_Name : Chunk_Name_Type)
                                 return Count
    is
        Chunk_Number : Positive;
    begin
        Chunk_Number := Find_Chunk_Number (Riff, Type_Name);
        return Chunk_Address (Riff, Chunk_Number) + 8;
        --  Add 8 to skip chunk header.
    end Chunk_Data_Address;

    procedure Close (Riff : in out RIFF_File) is
    begin
        Close (Riff.File);
    end Close;

    function Find_Chunk_Number (Riff : RIFF_File;
                                Type_Name : Chunk_Name_Type;
                                Starting_Number : Positive := 1)
                                return Natural
    is
        Header : Chunk_Header;
        I : Natural := Starting_Number;
    begin
        if Chunk_Count (Riff) < Starting_Number then
            --  Riff.Chunk_Count = 0 is Contemplated!
            --  (i.e. 0 < Starting_Number)
            return 0;
        end if;

        loop
            Header := Nth_Chunk_Header (Riff, I);
            I := I + 1;
            exit when I > Chunk_Count (Riff)
                or else Header.Chunk_Type = Type_Name;
        end loop;

        if Header.Chunk_Type = Type_Name then
            return I - 1;
        else
            return 0;
        end if;
    end Find_Chunk_Number;

    function Get_Chunk_Headers (Riff : RIFF_File) return Chunk_Header_Vector is
    begin
        return Riff.Chunk_Headers;
    end Get_Chunk_Headers;

    function Get_Header (Riff : RIFF_File) return RIFF_Header is
    begin
        return Riff.Header;
    end Get_Header;

    function Get_RIFF_Header (Path : String) return RIFF_Header is
        File : File_Type;
        Input_Stream : Stream_Access;
        Header : RIFF_Header;
    begin
        Open (File, In_File, Path);
        Input_Stream := Stream (File);
        Header :=  Read_RIFF_Header (Input_Stream);
        Close (File);

        return Header;
    end Get_RIFF_Header;

    procedure Goto_Nth_Chunk_Data (Riff : RIFF_File;
                                   Chunk_Number : Positive)
    is
    begin
        Set_Index (
            Riff.File,
            Chunk_Address (Riff, Chunk_Number) + 8
        );
    end Goto_Nth_Chunk_Data;

    procedure Initialise_All_Headers (Riff : in out RIFF_File) is
        Input_Stream : constant Stream_Access := Stream (Riff.File);
        Header : RIFF_Header;
    begin
        Header := Read_RIFF_Header (Input_Stream);
        Riff.Header := Header;
        Riff.Chunk_Headers := Read_All_Chunk_Headers (Riff.File,
                                                      Input_Stream);
    end Initialise_All_Headers;

    function Nth_Chunk_Data (Riff : RIFF_File;
                             Chunk_Number : Positive)
                            return Chunk_Data is
        Header : Chunk_Header;
        Input_Stream : Stream_Access;
    begin
        Header := Nth_Chunk_Header (Riff, Chunk_Number);
        Set_Index (Riff.File, Chunk_Address (Riff, Chunk_Number) + 8);
        Input_Stream := Stream (Riff.File);

        declare
            Data : Chunk_Data (1 .. Positive (Header.Length));
        begin
            Data := Read_Bytes (Input_Stream, Header.Length);
            return Data;
        end;
    end Nth_Chunk_Data;

    function Nth_Chunk_Data (Riff : RIFF_File;
                             Chunk_Number : Positive;
                             From, To : Positive)
                             return Chunk_Data
    is
        Header : Chunk_Header;
        Input_Stream : Stream_Access;
        From_U : constant Unsigned_32 := Unsigned_32 (From);
        To_U : constant  Unsigned_32 := Unsigned_32 (To);
    begin
        Header := Nth_Chunk_Header (Riff, Chunk_Number);

        if From_U > Header.Length
            or else Unsigned_32 (To) > Header.Length
            or else To < From
        then
            --  Invalid input!
            return Empty_Chunk_Data;
        end if;

        Set_Index (
            Riff.File,
            Chunk_Address (Riff, Chunk_Number) + 8 + Count (From)
        );
        Input_Stream := Stream (Riff.File);

        declare
            Data : Chunk_Data (1 .. Positive (Header.Length));
        begin
            Data := Read_Bytes (Input_Stream, To_U);
            return Data;
        end;
    end Nth_Chunk_Data;

    function Nth_Chunk_Header (Riff : RIFF_File; Chunk_Number : Positive)
                              return Chunk_Header is
        use Chunk_Header_Package;
    begin
        return Element (Riff.Chunk_Headers, Chunk_Number);
    end Nth_Chunk_Header;

    procedure Open (Path : String; Riff : out RIFF_File) is
    begin
        Open (Riff.File, In_File, Path);
        Initialise_All_Headers (Riff);
    end Open;

    function Read_All_Chunk_Headers (Input_File : File_Type;
                                     Input_Stream : Stream_Access)
                                    return Chunk_Header_Vector is
        use Chunk_Header_Package;
        Vector : Chunk_Header_Vector;
        Header : Chunk_Header;
    begin
        while not End_Of_File (Input_File) loop
            Header := Read_Chunk_Header (Input_Stream);
            Append (Vector, Header);
            Skip_Bytes (Input_Stream, Header.Length);
        end loop;

        return Vector;
    end Read_All_Chunk_Headers;

    function Read_Bytes (Input_Stream : Stream_Access;
                         Amount : Unsigned_32)
                        return Chunk_Data is
        subtype Datatype is Chunk_Data (1 .. Positive (Amount));

        --  Current : Unsigned_32 := 0;
        --  Datum : Byte;
        Data : Datatype;
    begin
        --  while Current < Amount loop
        --      Byte'Read (Input_Stream, Datum);
        --      Current := Current + 1;
        --  end loop;

        Datatype'Read (Input_Stream, Data);

        return Data;
    end Read_Bytes;

    function Read_Chunk_Header (Input_Stream : Stream_Access)
                               return Chunk_Header is
        Header : Chunk_Header;
    begin
        Chunk_Header'Read (Input_Stream, Header);
        return Header;
    end Read_Chunk_Header;

    function Read_RIFF_Header (Input_Stream : Stream_Access)
                              return RIFF_Header is
        Header : RIFF_Header;
    begin
        RIFF_Header'Read (Input_Stream, Header);
        return Header;
    end Read_RIFF_Header;

    procedure Skip_Bytes (Input_Stream : Stream_Access;
                          Amount : Unsigned_32) is
        Current : Unsigned_32 := 0;
        Data : Byte;
    begin
        while Current < Amount loop
            Byte'Read (Input_Stream, Data);
            Current := Current + 1;
        end loop;
    end Skip_Bytes;

    function To_String (Header : RIFF_Header) return String is
        Eol : constant Character := Ada.Characters.Latin_1.LF;
        Length_Kb : constant Unsigned_32 := Header.File_Length / 1024;
    begin
        return "RIFF header" & Eol
          & "  File length: " & Header.File_Length'Image
          & "(" & Length_Kb'Image & "Kb)" & Eol
          & "  Form type: " & Header.Form_Type;
    end To_String;

    function To_String (Header : Chunk_Header) return String is
        Eol : constant Character := Ada.Characters.Latin_1.LF;
    begin
        return "Chunk header" & Eol
          & "  Chunk type: " & String (Header.Chunk_Type) & Eol
          & "  Chunk length: " & Header.Length'Image;
    end To_String;

end RIFFs;
