--  riffs-data_readers.adb ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

package body RIFFs.Data_Readers is
    function Nth_Chunk_Data (Riff : RIFF_File;
                             Chunk_Number : Positive)
                            return Chunk_Data is
        Header : Chunk_Header;
        Input_Stream : Stream_Access;
    begin
        Header := Nth_Chunk_Header (Riff, Chunk_Number);
        Set_Index (Riff.File, Chunk_Address (Riff, Chunk_Number) + 8);
        Input_Stream := Stream (Riff.File);

        declare
            Data : Chunk_Data (1 .. Positive (Header.Length));
        begin
            Data := Read_Bytes (Input_Stream, Header.Length);
            return Data;
        end;
    end Nth_Chunk_Data;

    function Nth_Chunk_Data (Riff : RIFF_File;
                             Chunk_Number : Positive;
                             From, To : Positive)
                             return Chunk_Data
    is
        Header : Chunk_Header;
        Input_Stream : Stream_Access;
        From_U : constant Unsigned_32 := Unsigned_32 (From);
        To_U : constant  Unsigned_32 := Unsigned_32 (To);
    begin
        Header := Nth_Chunk_Header (Riff, Chunk_Number);

        if From_U > Header.Length
            or else Unsigned_32 (To) > Header.Length
            or else To < From
        then
            --  Invalid input!
            return Empty_Chunk_Data;
        end if;

        Set_Index (
            Riff.File,
            Chunk_Address (Riff, Chunk_Number) + 8 + Count (From)
        );
        Input_Stream := Stream (Riff.File);

        declare
            Data : Chunk_Data (1 .. Positive (Header.Length));
        begin
            Data := Read_Bytes (Input_Stream, To_U);
            return Data;
        end;
    end Nth_Chunk_Data;

    function Read_Bytes (Input_Stream : Stream_Access;
                         Amount : Unsigned_32)
                         return Chunk_Data
    is
        subtype Datatype is Chunk_Data (1 .. Positive (Amount));

        --  Current : Unsigned_32 := 0;
        --  Datum : Byte;
        Data : Datatype;
    begin
        --  while Current < Amount loop
        --      Byte'Read (Input_Stream, Datum);
        --      Current := Current + 1;
        --  end loop;

        Datatype'Read (Input_Stream, Data);

        return Data;
    end Read_Bytes;

end RIFFs.Data_Readers;
