--  get_chunk_fmt.adb ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Command_Line;
use Ada.Command_Line;
with RIFFs.WAVs.Formats;
with RIFFs.WAVs.Facts;
with RIFFs;

procedure Get_Chunk_Fmt is
    procedure Print_Arguments;

    procedure Print_Arguments is
    begin
        Put_Line ("get_chunck_fmt WAV_FILE_PATH");
    end Print_Arguments;

    The_Riff : RIFFs.RIFF_File;
    Wav_Format : RIFFs.WAVs.Formats.Format_Type;
    Samples_Per_Channel : Natural;
begin
    if Argument_Count < 1 then
        Print_Arguments;
        return;
    end if;

    RIFFs.Open (Argument (1), The_Riff);
    RIFFs.WAVs.Formats.Read_Format (The_Riff, Wav_Format);
    if RIFFs.Find_Chunk_Number (The_Riff, "fact") /= 0 then
        Samples_Per_Channel :=
            RIFFs.WAVs.Facts.Read_Samples_Per_Channel (The_Riff);
    else
        Samples_Per_Channel := 0;
    end if;
    RIFFs.Close (The_Riff);

    Put_Line (RIFFs.WAVs.Formats.To_String (Wav_Format));

    Put ("    Samples per channel: ");
    if Samples_Per_Channel > 0 then
        Put_Line (Samples_Per_Channel'Image);
    else
        Put_Line ("No info (chunk fact not found).");
    end if;
end Get_Chunk_Fmt;
