--  get_riff_header.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Command_Line;
use Ada.Command_Line;
with Ada.Strings.Fixed;
with Ada.Strings;
with RIFFs;
use RIFFs;
use RIFFs.Chunk_Header_Package;

procedure Get_Riff_Header is
    procedure Print_Arguments;

    procedure Print_Arguments is
    begin
        Put_Line ("get_riff_header WAV_FILE_PATH");
    end Print_Arguments;

    The_Riff : RIFF_File;
    Chunk_Number : Positive := 1;
begin
    if Argument_Count < 1 then
        Print_Arguments;
        return;
    end if;

    Open (Argument (1), The_Riff);

    Put_Line (To_String (Get_Header (The_Riff)));

    Put_Line ("Chunks:");
    for Header of Get_Chunk_Headers (The_Riff) loop
        Put (Ada.Strings.Fixed.Trim (Chunk_Number'Image,
                                     Ada.Strings.Both) & ": ");
        Put_Line (To_String (Header));

        Chunk_Number := Chunk_Number + 1;
    end loop;

    Close (The_Riff);
end Get_Riff_Header;
