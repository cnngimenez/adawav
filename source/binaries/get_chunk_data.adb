--  get_chunk_data.adb ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Command_Line;
use Ada.Command_Line;
with RIFFs;
use RIFFs;
with RIFFs.Data_Readers;

procedure Get_Chunk_Data is
    procedure Print_Arguments;

    type Byte is mod 2 ** 8;
    package Byte_Readers is new RIFFs.Data_Readers (
        Datum_Type => Byte,
        Empty_Datum => 0
    );

    procedure Print_Arguments is
    begin
        Put_Line ("get_chunck_data WAV_FILE_PATH CHUNK_NUMBER");
    end Print_Arguments;

    The_Riff : RIFF_File;
    Chunk_Number : Positive;
begin
    if Argument_Count < 2 then
        Print_Arguments;
        return;
    end if;

    Chunk_Number := Positive'Value (Argument (2));

    Open (Argument (1), The_Riff);

    declare
        Data : constant Byte_Readers.Chunk_Data :=
            Byte_Readers.Nth_Chunk_Data (The_Riff, Chunk_Number);
    begin
        for Datum of Data loop
            Ada.Text_IO.Put (Character'Val (Datum));
        end loop;
    end;

    Close (The_Riff);

end Get_Chunk_Data;
