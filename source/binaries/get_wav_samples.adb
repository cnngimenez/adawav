--  get_wav_samples.adb ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Command_Line;
use Ada.Command_Line;
with RIFFs;
with RIFFs.WAVs.Data;
with RIFFs.WAVs.Facts;

procedure Get_Wav_Samples is
    use all type RIFFs.Chunk_Name_Type;

    procedure Print_Help;

    procedure Print_Help is
    begin
        Put_Line ("Synopsis:");
        New_Line;
        Put_Line ("    get_wav_samples WAV_FILE CHUNK_NUMBER");
        New_Line;
        Put_Line ("Read samples from the WAV file.");
    end Print_Help;

    The_Riff : RIFFs.RIFF_File;
    Header : RIFFs.Chunk_Header;
    Samples_Per_Channel : Positive;
    Chunk_Number : Positive;
begin
    if Argument_Count /= 2 then
        Print_Help;
        return;
    end if;

    Chunk_Number := Positive'Value (Argument (2));
    RIFFs.Open (Argument (1), The_Riff);

    Samples_Per_Channel :=
        RIFFs.WAVs.Facts.Read_Samples_Per_Channel (The_Riff);

    Header := RIFFs.Nth_Chunk_Header (The_Riff, Chunk_Number);
    if Header.Chunk_Type /= "data" then
        Put_Line ("Warning: This header is not of data type!");
    end if;

    --  RIFFs.Goto_Nth_Chunk_Data (The_Riff, Chunk_Number);
    --  Float_Data := RIFFs.WAVs.Data.Get_Samples_Float (The_Riff, 1, 10);
    declare
        Float_Data : RIFFs.WAVs.Data.Float_Data_Type
            (1 .. Samples_Per_Channel + 1);
    begin
        Float_Data := RIFFs.WAVs.Data.Read_One_Channel_Floats (The_Riff, 1);

        for Datum of Float_Data loop
            Put_Line (Datum'Image);
        end loop;
    end;

    RIFFs.Close (The_Riff);
end Get_Wav_Samples;
