--  riffs-wavs-facts.ads ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

--
--  Subprograms to read the chunk data of type "facts".
--
package RIFFs.WAVs.Facts is

    function Read_Samples_Per_Channel (Riff : RIFF_File) return Positive;
    function Read_Samples_Per_Channel (Chunk : Chunk_Data) return Positive;

    Facts_Chunk_Not_Found_Exception : exception;
    Facts_Chunk_Wrong_Length_Exception : exception;

end RIFFs.WAVs.Facts;
