--  riffs.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Containers.Vectors;
with Ada.Streams.Stream_IO;
use Ada.Streams.Stream_IO;
with Interfaces;
use Interfaces;

--  Offset  Contents
--  (hex)
--  0000    'R', 'I', 'F', 'F'
--  0004    Length of the entire file - 8 (32-bit unsigned integer)
--  0008    form type (4 characters)
--
--  000C    first chunk type (4 character)
--  0010    first chunk length (32-bit unsigned integer)
--  0014    first chunk's data
--  ...     ...
--
--  Integer stored as little-endian representation.
--
package RIFFs is
    type Chunk_Name_Type is new String (1 .. 4);
    type Chunk_Header is record
        Chunk_Type : Chunk_Name_Type;
        Length : Unsigned_32;
    end record
    with Pack;

    Empty_Header : constant Chunk_Header := (
        Chunk_Type => "    ",
        Length => 0
    );

    function To_String (Header : Chunk_Header) return String;
    --  Return a string representation of the header data.

    package Chunk_Header_Package is new Ada.Containers.Vectors
      (Element_Type => Chunk_Header,
       Index_Type => Positive);

    subtype Chunk_Header_Vector is Chunk_Header_Package.Vector;

    type RIFF_Header is record
        RIFF_String : String (1 .. 4);
        File_Length : Unsigned_32;
        Form_Type : String (1 .. 4);
    end record
    with Pack;

    function Get_RIFF_Header (Path : String) return RIFF_Header;
    --  Retrieve the RIFF header from the given file. This is a shorthand
    --  function to open, read, and close the file.

    function To_String (Header : RIFF_Header) return String;

    type RIFF_File is limited private;

    procedure Open (Path : String; Riff : out RIFF_File);

    procedure Close (Riff : in out RIFF_File);

    function Get_Header (Riff : RIFF_File) return RIFF_Header;

    function Get_Chunk_Headers (Riff : RIFF_File)
                               return Chunk_Header_Vector;

    function Chunk_Count (Riff : RIFF_File) return Natural;

    function Nth_Chunk_Header (Riff : RIFF_File; Chunk_Number : Positive)
                              return Chunk_Header;

    function Find_Chunk_Number (Riff : RIFF_File;
                                Type_Name : Chunk_Name_Type;
                                Starting_Number : Positive := 1)
                                return Natural;
    --  Find the first chunk with the given type.
    --
    --  Start searching from the chunk numbered as Starting_Number (inclusive).
    --
    --  Return the chunk number to be used by Nth_Chunk_Header or
    --  Nth_Chunk_Data. If not found, return 0.

    --  --------------------------------------------------
    --  Retrieving data as Bytes
    --  --------------------------------------------------

    type Byte is mod 2 ** 8;
    type Chunk_Data is array (Positive range <>) of Byte;
    Empty_Chunk_Data : constant Chunk_Data := (0, 0);

    function Nth_Chunk_Data (Riff : RIFF_File;
                             Chunk_Number : Positive)
                            return Chunk_Data;
    --  Return all data inside a Chunk.
    --
    --  Warning: it can be too Big!

    function Nth_Chunk_Data (Riff : RIFF_File;
                             Chunk_Number : Positive;
                             From, To : Positive)
                             return Chunk_Data;
    --  Return a partial data from the Chunk.
    --
    --  This function is safer, as it returns a specific portion of Data
    --  that is inside the specified Chunk.

    procedure Goto_Nth_Chunk_Data (Riff : RIFF_File;
                                   Chunk_Number : Positive);

private
    type RIFF_File is limited record
        File : File_Type;
        Header : RIFF_Header;
        Chunk_Headers : Chunk_Header_Vector;
    end record;

    function Read_RIFF_Header (Input_Stream : Stream_Access)
                              return RIFF_Header;
    --  Read the RIFF header from the stream.

    function Read_Chunk_Header (Input_Stream : Stream_Access)
                              return Chunk_Header;
    --  Read the chunk header from the given stream.

    function Read_All_Chunk_Headers (Input_File : File_Type;
                                     Input_Stream : Stream_Access)
                                    return Chunk_Header_Vector;
    --  Read all the chunk headers from the given stream.

    procedure Skip_Bytes (Input_Stream : Stream_Access;
                          Amount : Unsigned_32);
    --  Read bytes from the given stream and discard them.

    function Read_Bytes (Input_Stream : Stream_Access;
                         Amount : Unsigned_32) return Chunk_Data;

    function Chunk_Address (Riff : RIFF_File;
                            Chunk_Number : Positive) return Unsigned_32;
    --  Return the starting position of the chunk in the File.

    function Chunk_Address (Riff : RIFF_File;
                            Chunk_Number : Positive) return Count;

    function Chunk_Data_Address (Riff : RIFF_File;
                                 Type_Name : Chunk_Name_Type)
                                 return Count;
    --  Return the starting position of the chunk data in File.

    procedure Initialise_All_Headers (Riff : in out RIFF_File);
    --  Initialise the Header and Chunk_Headers element in RIFF. RIFF.File must
    --  be assigned and opened.

end RIFFs;
